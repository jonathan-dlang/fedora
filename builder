#!/bin/bash

container=$(buildah from fedora:"${FEDORA_VERSION}")
buildah run "${container}"  /bin/sh << EOF
dnf install -y scl-utils scl-utils-build scl-utils-build-helpers git      \
                help2man bash-completion
mkdir -p "/opt/${VENDOR}"

echo '#!/bin/bash
source /usr/share/Modules/init/bash
source /etc/profile.d/scl-init.sh
exec "\$@"
' > "/opt/${VENDOR}/entrypoint"
chmod +x "/opt/${VENDOR}/entrypoint"

dnf clean all
rm -fr /var/cache/*
EOF
buildah config --author 'Jonathan MERCIER aka bioinfornatics' "${container}"
buildah config --comment 'Minimal Image to build SCLs' "${container}"
buildah config --entrypoint "/opt/${VENDOR}/entrypoint" "${container}"
buildah config --cmd /bin/bash "${container}"

echo "${container}"
